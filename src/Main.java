import domain.recipe.Ingredient;
import domain.recipe.Recipe;
import domain.team.ProductTeamMember;
import domain.team.Supplier;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        /*
               -- Suppliers --
        */

        Supplier sup = new Supplier(1L, "PP");
        Supplier sup2 = new Supplier(2L, "DD");
        Supplier sup3 = new Supplier(3L, "FF");

        /*
               -- Ingredients --
        */

        Ingredient cucamber = new Ingredient("cucamber", 12L, 0.5D, 100D);
        Ingredient lettuce = new Ingredient("lettuce", 10L, 0.5D, 150D);
        Ingredient cheese = new Ingredient("cheese", 192L, 0.7D, 50D);
        Ingredient bread = new Ingredient("bread", 190L, 0.7D, 60D);
        Ingredient chicken = new Ingredient("chick", 191L, 1D, 100D);
        Ingredient sliceOfDemocracy = new Ingredient("sliceOfDemocracy", 100L, 100D, 1D);
        Ingredient beef = new Ingredient("beef", 12983L, 1D, 1000D);


        /*
               -- DATES --
        */

        Calendar cldr = Calendar.getInstance();
        cldr.set(2023, Calendar.NOVEMBER, 15);
        Date date1 = cldr.getTime();
        cldr.set(2023, Calendar.NOVEMBER, 20);
        Date date2 = cldr.getTime();


        /*
               -- Sup --
        */

        cucamber.addSupplier(sup);
        lettuce.addSupplier(sup);
        cheese.addSupplier(sup);
        bread.addSupplier(sup);
        chicken.addSupplier(sup);
        sliceOfDemocracy.addSupplier(sup);
        beef.addSupplier(sup);

        cucamber.addSupplier(sup2);
        lettuce.addSupplier(sup2);
        cheese.addSupplier(sup2);
        bread.addSupplier(sup2);
        chicken.addSupplier(sup2);
        sliceOfDemocracy.addSupplier(sup2);
        beef.addSupplier(sup2);


        cucamber.addSupplier(sup3);
        lettuce.addSupplier(sup3);
        cheese.addSupplier(sup3);
        bread.addSupplier(sup3);
        chicken.addSupplier(sup3);
        sliceOfDemocracy.addSupplier(sup3);
        beef.addSupplier(sup3);


        sup.setIngredientPrices(cucamber, date1, 150D);
        sup.setIngredientPrices(lettuce, date2, 400D);
        sup.setIngredientPrices(cheese, date1, 150D);
        sup.setIngredientPrices(bread, date1, 150D);
        sup.setIngredientPrices(chicken, date1, 150D);
        sup.setIngredientPrices(sliceOfDemocracy, date1, 150D);
        sup.setIngredientPrices(beef, date1, 150D);


        sup2.setIngredientPrices(cucamber, date1, 50D);
        sup2.setIngredientPrices(lettuce, date1, 30D);
        sup2.setIngredientPrices(cheese, date1, 70D);
        sup2.setIngredientPrices(bread, date1, 70D);
        sup2.setIngredientPrices(chicken, date2, 30D);
        sup2.setIngredientPrices(sliceOfDemocracy, date1, 70D);
        sup2.setIngredientPrices(beef, date2, 30D);


        sup3.setIngredientPrices(cucamber, date2, 74D);
        sup3.setIngredientPrices(lettuce, date1, 95D);
        sup3.setIngredientPrices(cheese, date1, 74D);
        sup3.setIngredientPrices(bread, date2, 86D);
        sup3.setIngredientPrices(chicken, date1, 74D);
        sup3.setIngredientPrices(sliceOfDemocracy, date1, 195D);
        sup3.setIngredientPrices(beef, date1, 80D);


         /*
               -- Recipe --
        */


        Recipe godSaveMeFromThisSalad = new Recipe("God Save Me From This Salad", 1984L);

        godSaveMeFromThisSalad.addIngredient(cucamber, 3D);
        godSaveMeFromThisSalad.addIngredient(lettuce, 5D);
        godSaveMeFromThisSalad.addIngredient(cheese, 1D);
        godSaveMeFromThisSalad.addIngredient(sliceOfDemocracy, 1D);
        godSaveMeFromThisSalad.addIngredient(chicken, 3D);

        Recipe pizzaComrad = new Recipe("Pizza Comrad aka Red Star in ya mouth", 1954L);

        pizzaComrad.addIngredient(bread, 5D);
        pizzaComrad.addIngredient(cheese, 3D);
        pizzaComrad.addIngredient(beef, 2D);
        pizzaComrad.addIngredient(sliceOfDemocracy, 1D);

        List<Recipe> recipesDB = new ArrayList<>();
        recipesDB.add(pizzaComrad);
        recipesDB.add(godSaveMeFromThisSalad);


         /*
               -- Test --
        */


        ProductTeamMember member = new ProductTeamMember("John Doe");
        member.addRecipeDB(recipesDB);

        System.out.println(member.getCostById(1984L, date1));
    }
}
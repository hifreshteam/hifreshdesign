package domain.recipe;

import domain.team.Supplier;

import java.util.*;

public class Ingredient {
    private String name;
    private Long ingredientID;
    private Double defaultProfitablePrice;

    // Interesting part
    private Double ingredientWeight;
    private List<Supplier> suppliersList;

    public Ingredient(String name, Long ingredientID, Double ingredientWeight, Double defaultProfitablePrice) {
        this.name = name;
        this.ingredientID = ingredientID;
        this.ingredientWeight = ingredientWeight;
        this.defaultProfitablePrice = defaultProfitablePrice;
        this.suppliersList = new ArrayList<>();
    }


    public void addSupplier(Supplier supplier) {
        suppliersList.add(supplier);
    }

    public Double getAveragePriceAtDate(Date date) {
        Double totalPrice = 0D;
        int countOfSupps = 0;

        for (Supplier supplier : suppliersList) {
            Double price = supplier.getIngredientPrice(this, date);
            if (price > 0) {
                totalPrice += price;
                countOfSupps++;
            }
        }

        return (countOfSupps > 0) ? totalPrice / countOfSupps : 0D;
    }


    // getters and setters

    public String getName() {
        return name;
    }

    public Long getIngredientID() {
        return ingredientID;
    }

    public Double getIngredientWeight() {
        return ingredientWeight;
    }

    public void setDefaultProfitablePrice(Double defaultProfitablePrice) {
        this.defaultProfitablePrice = defaultProfitablePrice;
    }

    public void setIngredientWeight(Double ingredientWeight) {
        this.ingredientWeight = ingredientWeight;
    }
}

package domain.recipe;

import java.util.*;

public class Recipe {
    private String recipeName;
    private Long recipeID;
    private Map<Ingredient, Double> ingredientMap;

    public Recipe(String recipeName, Long recipeID) {
        this.recipeName = recipeName;
        this.recipeID = recipeID;
        this.ingredientMap = new HashMap<>();
    }

    // Ingredients manipulation
    public void addIngredient(Ingredient ingredient, Double amount) {
        ingredientMap.put(ingredient, amount);
    }

    public Double calculateCostAtDate(Date date) {
        Double totalCost = 0D;

        for (Map.Entry<Ingredient, Double> entry : ingredientMap.entrySet()) {
            totalCost += entry.getKey().getAveragePriceAtDate(date) * entry.getValue();
        }

        return totalCost;
    }

    public Double calculateWeight() {
        Double totalWeight = 0D;

        for (Map.Entry<Ingredient, Double> entry : ingredientMap.entrySet()) {
            totalWeight += entry.getKey().getIngredientWeight() * entry.getValue();
        }

        return totalWeight;
    }

    public Map<Ingredient, Double> getIngredientsMap() {
        return ingredientMap;
    }

    public Long getRecipeID() {
        return recipeID;
    }


}

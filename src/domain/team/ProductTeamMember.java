package domain.team;

import domain.recipe.Recipe;

import java.util.*;

public class ProductTeamMember extends Employee {
    private String name;
    private List<Recipe> recipeDB;

    public ProductTeamMember(String name) {
        this.name = name;
    }

    public void addRecipeDB(List<Recipe> recipeDB) {
        this.recipeDB = recipeDB;
    }

    public Double getCostById(Long recipeID, Date date) {
        for (Recipe recipe : recipeDB) {
            if (recipe.getRecipeID().equals(recipeID)) {
                return recipe.calculateCostAtDate(date);
            }
        }
        return 0D;
    }

}

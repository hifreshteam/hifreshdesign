package domain.team;

import domain.recipe.Ingredient;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Supplier extends Person {
    private Long supplierID;
    private String location;
    private Map<Ingredient, Map<Date, Double>> ingredientPrices;

    public Supplier(Long supplierID, String location) {
        this.location = location;
        this.supplierID = supplierID;
        this.ingredientPrices = new HashMap<>();
    }

    public void setIngredientPrices(Ingredient ingredient, Date date, Double price) {
        ingredientPrices.computeIfAbsent(ingredient, k -> new HashMap<>()).put(date, price);
    }

    public double getIngredientPrice(Ingredient ingredient, Date date) {
        return ingredientPrices.getOrDefault(ingredient, Collections.emptyMap()).getOrDefault(date, 0.0);
    }

    public Long getSupplierId() {
        return supplierID;
    }

    public Map<Ingredient, Map<Date, Double>> getIngredientPrices() {
        return ingredientPrices;
    }


}
